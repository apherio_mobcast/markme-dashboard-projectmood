import store from 'vuex-store'
let palette = store.getters.palette


export default {
  labels: ['Happy', 'Sad', 'Netural'],
  datasets: [{
    label: 'Organization Mood',
    backgroundColor: [palette.danger, palette.info, palette.success],
    data: [2478, 5267, 734]
  }]
}
