import store from 'vuex-store'

let palette = store.getters.palette



export default {
  labels: ['Sad', 'Neutral', 'Happy'],
  datasets: [{
    label: 'How is the Company Doing',
    backgroundColor: [palette.info, palette.warning, palette.primary],
    data: [25, 70, 500]
  }]
}
